﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using trivia_client.Views;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System;
using System.IO;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace trivia_client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Socket soc;
        public MainWindow()
        {
            InitializeComponent();
            StartClient();
            frame.NavigationService.Navigate(new SignIn(this));
        }

        public void dc()
        {
            SendMSGByProt("{}", 0);
            soc.Close();
        }

        public void SendMSGByProt(string s,int code)
        {
            List<byte> toSend = new List<byte>();
            byte[] intBytes;
            byte[] stringBytes;
            toSend.Add((byte)code);
            intBytes = BitConverter.GetBytes(s.Length);
            for (int i = 0; i < intBytes.Length; i++)
            {
                toSend.Add(intBytes[i]);
            }
            stringBytes = Encoding.ASCII.GetBytes(s);
            for (int i = 0; i < stringBytes.Length; i++)
            {
                toSend.Add(stringBytes[i]);
            }
            try
            {
                soc.Send(toSend.ToArray());
            }
            catch
            {
                MessageBox.Show("Connection to server lost!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public string getMSG()
        {
            byte[] size = new byte[4];
            int bufSize = 0;
            byte[] buff;
            soc.Receive(size,4,SocketFlags.None);
            bufSize = BitConverter.ToInt32(size, 0);
            buff = new byte[bufSize];

            try
            {
                soc.Receive(buff, bufSize, SocketFlags.None);
            }
            catch
            {
                MessageBox.Show("Connection to server lost!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return UTF8Encoding.ASCII.GetString(buff);
        }

        public void StartClient()
        {
            byte[] bytes = new byte[1024];
            byte[] ip = new byte[4];
            int port;
            string configPath = "config.txt";
            if (!File.Exists(configPath))
            {
                string[] lines = { "[config]", "ip : 127.0.0.1", "port: 8820" };
                System.IO.File.WriteAllLines(configPath, lines);
            }
            try
            {
                var parser = new ConfigurationParser.Parser(configPath);
                var ipStr = parser.GetString("config", "ip");
                port = parser.GetInt("config", "port");
                ip[0] = (byte)Int32.Parse(ipStr.Split('.')[0]);
                ip[1] = (byte)Int32.Parse(ipStr.Split('.')[1]);
                ip[2] = (byte)Int32.Parse(ipStr.Split('.')[2]);
                ip[3] = (byte)Int32.Parse(ipStr.Split('.')[3]);
            }
            catch
            {
                return;
            }
            try
            {
                // Connect to a Remote server  
                // Get Host IP Address that is used to establish a connection  
                // In this case, we get one IP address of localhost that is IP : 127.0.0.1  
                // If a host has multiple addresses, you will get a list of addresses  
                IPAddress ipAddress = new IPAddress(ip);
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

                // Create a TCP/IP  socket.    
                soc = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                while (true)
                {
                    // Connect the socket to the remote endpoint. Catch any errors.    
                    try
                    {
                        // Connect to Remote EndPoint  
                        soc.Connect(remoteEP);
                        break;
                    }
                    catch (ArgumentNullException ane)
                    {
                        log.Text += "ArgumentNullException : {0}" + ane.ToString() + '\n';
                    }
                    catch (SocketException se)
                    {
                        log.Text += "SocketException : {0}" + se.ToString() + '\n';
                    }
                    catch (Exception e)
                    {
                        log.Text += "Unexpected exception : {0}" + e.ToString() + '\n';
                    } 
                }

            }
            catch (Exception e)
            {
                log.Text += e.ToString() + '\n';
            }
        }

        private void Frame_Navigated(object sender, NavigationEventArgs e)
        {

        }
    }
}
