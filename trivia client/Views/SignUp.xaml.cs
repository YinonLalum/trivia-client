﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace trivia_client.Views
{
    /// <summary>
    /// Interaction logic for SignUp.xaml
    /// </summary>
    public partial class SignUp : UserControl
    {
        private MainWindow window;
        public SignUp(MainWindow window)
        {
            InitializeComponent();
            window.Height = this.Height;
            window.Width = this.Width;
            this.window = window;
        }

        private void Sign_Up(object sender, RoutedEventArgs e)
        {
            Brush color = this.Bd.Background;

            if (Pass1.Password != Pass2.Password)
            { 
                this.Bd.Background = Brushes.Red;
                MessageBox.Show("Passwords don't match!!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Bd.Background = color;
                return;
            }

            try
            {
                System.Net.Mail.MailAddress m = new MailAddress(Email.Text);
            }
            catch (FormatException)
            {
                this.Bd.Background = Brushes.Red;
                MessageBox.Show("Email is in bad format!!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Bd.Background = color;
                return;
            }

            string msg = "{\"username\": \"" + UserName.Text + "\",\"password\": \"" + Pass1.Password + "\",\"email\": \"" + Email.Text + "\"}";
            window.SendMSGByProt(msg, 2);

            string ret = window.getMSG();
            window.log.Text += ret;

            if (ret.Contains(":)"))
                window.frame.NavigationService.Navigate(new Menu(window, UserName.Text));
            else
            {
                this.Bd.Background = Brushes.Red;
                MessageBox.Show("Failed to signup!!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Bd.Background = color;
            }
        }

        private void Sign_Ip(object sender, RoutedEventArgs e)
        {
            window.frame.NavigationService.Navigate(new SignIn(window));
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            window.dc();
            System.Windows.Application.Current.Shutdown();
        }
    }
}
