﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Web.Script.Serialization;
using System.Threading;

namespace trivia_client.Views
{
    /// <summary>
    /// Interaction logic for RoomBrowser.xaml
    /// </summary>

    public partial class RoomBrowser : UserControl
    {

        Thread updateList; 
        public Menu menu;
        private int selcetedRoomID;
        private RoomData data;
        public RoomBrowser(Menu menu)
        {
            InitializeComponent();
            menu.window.Height = 600;
            menu.window.Width = 800;

            this.selcetedRoomID = -1;
            this.menu = menu;
            menu.window.Height = this.Height;
            menu.window.Width = this.Width;
            this.Username.Content = menu.Username.Content;

            updateList = new Thread(updateRooms);
            updateList.Start();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            menu.window.Height = 600;
            menu.window.Width = 500;
            updateList.Abort();
            menu.window.frame.NavigationService.Navigate(menu);
        }

        private void updateRooms()
        {
            while (true)
            {
                this.Dispatcher.Invoke(() =>
                {
                    Button_Click_1(null, null);
                });
                Thread.Sleep(1000);
            }
        }
        private void room_Click(object sender, RoutedEventArgs e)
        {
            data = (RoomData)((Button)sender).Tag;
            room_name.Content = data.name;
            this.questions.Content = "Number of questions - " + data.questions;
            this.time.Content = "Time per question - " + data.time;

            menu.window.SendMSGByProt("{\"roomid\" : " + data.id +"}", 5);
            string asJson = menu.window.getMSG();

            List<string> players = new JavaScriptSerializer().Deserialize<List<string>>(asJson);

            this.players.Content = "Current users - " + players.Count + "/" + data.players;
            panel1.Children.Clear();
            for (int i = 0;i < players.Count; i++)
            {
                panel1.Children.Add(new Label { Content = players[i], Height = 55, Width = 165, HorizontalAlignment = HorizontalAlignment.Left, FontSize = 24, Foreground = Brushes.WhiteSmoke });
            }
            this.selcetedRoomID = data.id;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            menu.window.SendMSGByProt("{\"roomid\": " + this.selcetedRoomID + "}",6);
            string resp = menu.window.getMSG();

            if (resp.Contains(":("))
                MessageBox.Show(resp, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            else
                menu.window.frame.NavigationService.Navigate(new RoomMember(menu,data));
            updateList.Abort();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            panel.Children.Clear();
            menu.window.SendMSGByProt("{}", 4);
            string asJson = menu.window.getMSG();

            if (asJson.Contains(":("))
            {
                MessageBox.Show(asJson, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            List<RoomData> s = new JavaScriptSerializer().Deserialize<List<RoomData>>(asJson);

            for (int i = 0; i < s.Count; i++)
            {
                Button but = new Button { Height = 30, Width = 90, Margin = new Thickness(0, 20, 0, 0), Content = s[i].name, Tag = s[i] };
                but.Click += room_Click;
                but.Style = (Style)this.FindResource("RoundCorner2");
                panel.Children.Add(but);
            }
        }
    }
    public class RoomData
    {
        public int id { get; set; }
        public string name { get; set; }
        public int players { get; set; }
        public int time { get; set; }
        public int active { get; set; }
        public int questions { get; set; }
    };
}
