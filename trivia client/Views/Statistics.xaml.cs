﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace trivia_client.Views
{
    /// <summary>
    /// Interaction logic for Stats.xaml
    /// </summary>
    public partial class Statistics : UserControl
    {
        public Menu menu;
        public Statistics(Menu menu,List<PlayerResult> results)
        {
            menu.window.Height = 600;
            menu.window.Width = 800;

            PlayerResult p;
            this.menu = menu;
            InitializeComponent();
            this.Username.Content = menu.Username.Content;
            for(int i = 0; i<results.Count;i++)
            {
                p = results[i];
                string content = "User: " + p.username + "\tCorrect Answer Count: " + p.correctAnswerCount + "\tWrong Answer Count: " + p.wrongAnswerCount + "\tAverage Answer Time: " + p.averageAnswerTime;
                panel1.Children.Add(new Label { Content = content, Height = 55, HorizontalAlignment = HorizontalAlignment.Left, FontSize = 15, Foreground=Brushes.White });
            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            menu.window.Height = 600;
            menu.window.Width = 500;
            menu.window.frame.NavigationService.Navigate(menu);
        }
    }
}
