﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Web.Script.Serialization;

namespace trivia_client.Views
{
    /// <summary>
    /// Interaction logic for RoomAdmin.xaml
    /// </summary>
    public partial class RoomAdmin : UserControl
    {
        public Menu menu;
        private RoomData data;
        public RoomAdmin(Menu menu, RoomData data)
        {
            this.menu = menu;
            InitializeComponent();
            this.data = data;
            this.Username.Content = menu.Username.Content;
            room_name.Content = data.name;
            this.questions.Content = "Number of questions - " + data.questions;
            this.time.Content = "Time per question - " + data.time;
            this.players.Content = "Current users - 0/" + data.players;
            panel1.Children.Clear();
            Thread t = new Thread(ListenForUpdates);
            t.Start();
        }

        private void Button_Click(object sender, RoutedEventArgs e) //start
        {
            this.menu.window.SendMSGByProt("{}", 13);
            string resp = this.menu.window.getMSG();
            if (resp.Contains(":("))
            {
                MessageBox.Show(resp, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            menu.window.frame.NavigationService.Navigate(new Game(menu,data));
        }

        private void ListenForUpdates()
        {
            RoomState state  = new RoomState();
            state.hasGameBegun = false;
            while(!state.hasGameBegun)
            {
                this.menu.window.SendMSGByProt("{}", 11);
                string resp = this.menu.window.getMSG();
                if (resp.Contains(":("))
                    return;
                state = new JavaScriptSerializer().Deserialize<RoomState>(resp);


                try
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        this.panel1.Children.Clear();
                    });
                }
                catch (Exception)
                {
                    return;
                } 
                for(int i = 0; i < state.players.Count; i++)
                {
                    try
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            this.panel1.Children.Add(new Label { Content = state.players[i], Height = 55, Width = 165, HorizontalAlignment = HorizontalAlignment.Left, FontSize = 24 });
                         });
                    }
                    catch (Exception)
                    {
                        return;
                    }
                }
                System.Threading.Thread.Sleep(500);
            }
        }
        private void Button_Click_1(object sender, RoutedEventArgs e) //close
        {
            this.menu.window.SendMSGByProt("{}", 12);
            string resp = this.menu.window.getMSG();
            if (resp.Contains(":("))
            {
                MessageBox.Show(resp, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            menu.window.Height = 600;
            menu.window.Width = 500;
            menu.window.frame.NavigationService.Navigate(menu);
        }

        private class RoomState
        {
            public bool hasGameBegun { get; set; }
            public List<string> players { get; set; }
            public int questionCount { get; set; }
            public int ansTimeOut { get; set; }
        };

    }
}
