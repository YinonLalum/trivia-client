﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace trivia_client.Views
{
    /// <summary>
    /// Interaction logic for CreateRoom.xaml
    /// </summary>
    public partial class CreateRoom : UserControl
    {
        public Menu menu;
        public CreateRoom(Menu menu)
        {
            InitializeComponent();
            menu.window.Height = 600;
            menu.window.Width = 800;
            this.menu = menu;
            this.Username.Content = menu.Username.Content;


        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            menu.window.Height = 600;
            menu.window.Width = 500;
            menu.window.frame.NavigationService.Navigate(menu);
        }

        private void Create_room_Click(object sender, RoutedEventArgs e)
        {
            string s;
            s = "{ \"roomName\": \"" + name.Text + "\"," +
                " \"maxUsers\": " + players.Text + "," +
                " \"questionCount\": " + questions.Text + "," +
                " \"answerTimeout\" : " + time.Text + "}";
            RoomData data = new RoomData();
            data.active = 0;
            data.name = name.Text;
            data.players = System.Convert.ToInt32(players.Text);
            data.questions = System.Convert.ToInt32(questions.Text);
            data.time = System.Convert.ToInt32(time.Text);
            menu.window.SendMSGByProt(s, 7);
            string resp = menu.window.getMSG();
            if (resp.Contains(":)"))
                menu.window.frame.NavigationService.Navigate(new RoomAdmin(menu,data));
            else
                MessageBox.Show(resp, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

        }
    }

}
