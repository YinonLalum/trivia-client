﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace trivia_client.Views
{
    /// <summary>
    /// Interaction logic for ChangePassword.xaml
    /// </summary>
    public partial class ChangePassword : UserControl
    {
        private MainWindow window;
        public ChangePassword(MainWindow window)
        {
            InitializeComponent();
            this.window = window;
            window.Height = this.Height;
            window.Width = this.Width;
        }

        private void Change_Password(object sender, RoutedEventArgs e)
        {
            Brush color = this.Bd.Background;

            if (Pass1.Password == Pass2.Password)
            {
                string msg = "{\"username\": \"" + UserName.Text + "\",\"password\": \"" + CurrPass.Password + "\"}";
                string msg2 = "{\"username\": \"" + UserName.Text + "\",\"password\": \"" + Pass1.Password + "\"}";

                // Connecting
                window.SendMSGByProt(msg, 1);
                string ret = window.getMSG();
                window.log.Text += ret;
                

                if (ret.Contains(":)"))
                {
                    // Disconnecting
                    window.SendMSGByProt("{}", 3);

                    // Changing the password
                    window.SendMSGByProt(msg2, 19);
                    ret = window.getMSG();
                    window.log.Text += ret;

                    if(ret.Contains(":("))
                    {
                        this.Bd.Background = Brushes.Red;
                        MessageBox.Show("Failed to change password!!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        this.Bd.Background = color;
                    }
                    else
                    {
                        MessageBox.Show("Password changed successfully");
                        window.frame.NavigationService.Navigate(new SignIn(window));
                    }
                }
                else
                {
                    this.Bd.Background = Brushes.Red;
                    MessageBox.Show("Incorrect credentials!!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    this.Bd.Background = color;
                }
            }
            else
            {
                this.Bd.Background = Brushes.Red;
                MessageBox.Show("Passwords don't match!!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Bd.Background = color;
            }


            //if (ret.Contains(":)"))
            //    window.frame.NavigationService.Navigate(new Menu(window, UserName.Text));
            //else
            //    MessageBox.Show("Failed to signup!!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            window.dc();
            System.Windows.Application.Current.Shutdown();
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            window.frame.NavigationService.Navigate(new SignIn(window));
        }
    }
}
