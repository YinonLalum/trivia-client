﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace trivia_client.Views
{
    /// <summary>
    /// Interaction logic for RoomMember.xaml
    /// </summary>
    public partial class RoomMember : UserControl
    {
        public Menu menu;
        private RoomData data;
        public RoomMember(Menu menu,RoomData data)
        {
            this.menu = menu;
            InitializeComponent();
            this.data = data;
            this.Username.Content = menu.Username.Content;
            room_name.Content = data.name;
            this.questions.Content = "Number of questions - " + data.questions;
            this.time.Content = "Time per question - " + data.time;
            this.players.Content = "Current users - 0/" + data.players;
            panel1.Children.Clear();
            Thread t = new Thread(ListenForUpdates);
            t.Start();
        }

        private void Button_Click(object sender, RoutedEventArgs e) //start
        {
            this.menu.window.SendMSGByProt("{}", 10);
            string resp = this.menu.window.getMSG();
            if (resp.Contains(":("))
            {
                MessageBox.Show(resp, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            //Height="600" Width="530"
            menu.window.Height = 600;
            menu.window.Width = 500;
            menu.window.frame.NavigationService.Navigate(menu);
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            menu.window.Height = 600;
            menu.window.Width = 500;
            menu.window.frame.NavigationService.Navigate(menu);
        }

        private void ListenForUpdates()
        {
            RoomState state = new RoomState();
            state.hasGameBegun = false;
            while (!state.hasGameBegun)
            {
                this.menu.window.SendMSGByProt("{}", 11);
                string resp = this.menu.window.getMSG();
                if (resp.Contains(":("))
                {
                    try
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            this.menu.window.frame.NavigationService.Navigate(menu);
                        });
                    }
                    catch (Exception)
                    {
                        return;
                    }
                    return;
                }
                state = new JavaScriptSerializer().Deserialize<RoomState>(resp);


                try
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        this.panel1.Children.Clear();
                    });
                }
                catch (Exception)
                {
                    return;
                }
                for (int i = 0; i < state.players.Count; i++)
                {
                    try
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            this.panel1.Children.Add(new Label { Content = state.players[i], Height = 55, Width = 165, HorizontalAlignment = HorizontalAlignment.Left, FontSize = 24 });
                        });
                    }
                    catch (Exception)
                    {
                        return;
                    }
                }
                if(state.hasGameBegun)
                {
                    try
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            this.menu.window.frame.NavigationService.Navigate(new Game(menu,data));
                        });
                    }
                    catch(Exception)
                    {
                        return;
                    }
                }
                System.Threading.Thread.Sleep(500);
            }
        }

        private class RoomState
        {
            public bool hasGameBegun { get; set; }
            public List<string> players { get; set; }
            public int questionCount { get; set; }
            public int ansTimeOut { get; set; }
        };

    }
}
