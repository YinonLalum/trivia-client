﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace trivia_client.Views
{
    /// <summary>
    /// Interaction logic for ForgotPassword.xaml
    /// </summary>
    public partial class ForgotPassword : UserControl
    {
        private MainWindow window;
        public ForgotPassword(MainWindow window)
        {
            InitializeComponent();
            window.Height = this.Height;
            window.Width = this.Width;
            this.window = window;
        }

        private void Send_Mail(object sender, RoutedEventArgs e)
        {
            Brush color = this.Bd.Background;
            string msg = "{\"username\": \"" + UserName.Text + "\"}";
            window.SendMSGByProt(msg, 18);

            string ret = window.getMSG();
            window.log.Text += ret;


            if (ret.Contains(":)"))
                window.frame.NavigationService.Navigate(new ChangePasswordByCode(window, UserName.Text));
            else
            {
                this.Bd.Background = Brushes.Red;
                MessageBox.Show("There is no such user!!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Bd.Background = color;
            }
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            window.dc();
            System.Windows.Application.Current.Shutdown();
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            window.frame.NavigationService.Navigate(new SignIn(window));
        }
    }
}
