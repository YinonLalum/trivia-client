﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace trivia_client.Views
{
    /// <summary>
    /// Interaction logic for SignIn.xaml
    /// </summary>
    public partial class SignIn : UserControl
    {
        MainWindow window;
        public SignIn(MainWindow window)
        {
            InitializeComponent();
            this.window = window;

            window.Height = this.Height;
            window.Width = this.Width;
        }

        private void Sign_In(object sender, RoutedEventArgs e)
        {
            Brush color = this.Bd.Background;
            string msg = "{\"username\": \"" + UserName1.Text + "\",\"password\": \"" + PassIn.Password + "\"}";
            window.SendMSGByProt(msg, 1);
            string ret = window.getMSG();
            window.log.Text += ret;

            if (ret.Contains(":)"))
                window.frame.NavigationService.Navigate(new Menu(window, UserName1.Text));
            else
            {
                this.Bd.Background = Brushes.Red;
                MessageBox.Show("Incorrect credentials!!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Bd.Background = color;
            }
        }

        private void Forgot_Password(object sender, RoutedEventArgs e)
        {
            window.frame.NavigationService.Navigate(new ForgotPassword(window));
        }

        private void Change_Password(object sender, RoutedEventArgs e)
        {
            window.frame.NavigationService.Navigate(new ChangePassword(window));
        }

        private void Sign_Up(object sender, RoutedEventArgs e)
        {
            window.frame.NavigationService.Navigate(new SignUp(window));
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            window.dc();
            System.Windows.Application.Current.Shutdown();
        }
    }
}
