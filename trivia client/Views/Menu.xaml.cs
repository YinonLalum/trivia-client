﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Web.Script.Serialization;

namespace trivia_client.Views
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : UserControl
    {
        public MainWindow window;
        public Menu(MainWindow window, string username)
        {
            InitializeComponent();
            window.Height = 600;
            window.Width = 500;
            this.window = window;
            Username.Content = username;
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            window.dc();
            System.Windows.Application.Current.Shutdown();
        }

        private void _out_Click(object sender, RoutedEventArgs e)
        {
            window.SendMSGByProt("{}", 3);
            window.frame.NavigationService.Navigate(new SignIn(window));
        }

        private void Score_Click(object sender, RoutedEventArgs e)
        {
            window.SendMSGByProt("{}", 8);
            string asJson = window.getMSG();

            List<Highscore> scores = new JavaScriptSerializer().Deserialize<List<Highscore>>(asJson);
            window.frame.NavigationService.Navigate(new BestScores(this, scores));
        }

        private void Status_Click(object sender, RoutedEventArgs e)
        {
            window.SendMSGByProt("{}", 9);
            string asJson = window.getMSG();

            Stats s = new JavaScriptSerializer().Deserialize<Stats>(asJson);
            window.frame.NavigationService.Navigate(new Status(this, s));
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            CreateRoom wind = new CreateRoom(this);
            window.frame.NavigationService.Navigate(wind);
        }

        private void Join_Click(object sender, RoutedEventArgs e)
        {
            window.frame.NavigationService.Navigate(new RoomBrowser(this));
        }
    }
}

public class Stats
{
    public float avg { get; set; }
    public int games { get; set; }
    public int right { get; set; }
    public int wrong { get; set; }
}

public class Highscore
{
    public string user { get; set; }
    public int score { get; set; }
}