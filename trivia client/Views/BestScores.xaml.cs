﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace trivia_client.Views
{
    /// <summary>
    /// Interaction logic for BestScores.xaml
    /// </summary>
    public partial class BestScores : UserControl
    {
        public Menu menu;
        public BestScores(Menu menu, List<Highscore> scores)
        {
            this.menu = menu;
            menu.window.Height = 600;
            menu.window.Width = 800;

            List<Highscore> sorted = scores.OrderBy(o => o.score).ToList();
            InitializeComponent();

            place3.Content = sorted[0].user + " - " + sorted[0].score;
            place2.Content = sorted[1].user + " - " + sorted[1].score;
            place1.Content = sorted[2].user + " - " + sorted[2].score;
            this.Username.Content = menu.Username.Content;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            menu.window.Height = 600;
            menu.window.Width = 500;
            menu.window.frame.NavigationService.Navigate(menu);
        }
    }
}
