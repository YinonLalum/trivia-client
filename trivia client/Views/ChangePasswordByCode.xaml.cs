﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace trivia_client.Views
{
    /// <summary>
    /// Interaction logic for ChangePasswordByCode.xaml
    /// </summary>
    public partial class ChangePasswordByCode : UserControl
    {
        private MainWindow window;
        private string user;
        public ChangePasswordByCode(MainWindow window, string user)
        {
            InitializeComponent();
            window.Height = this.Height;
            window.Width = Width;
            this.window = window;
            this.user = user;
        }

        private void Change_Password(object sender, RoutedEventArgs e)
        {
            Brush color = this.Bd.Background;

            if (Pass1.Password != Pass2.Password)
            {
                this.Bd.Background = Brushes.Red;
                MessageBox.Show("Passwords don't match!!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Bd.Background = color;
                return;
            }

            string msg = "{\"username\": \"" + user + "\",\"password\": \"" + Pass1.Password + "\",\"code\": \"" + Code.Text + "\"}";
            window.SendMSGByProt(msg, 20);

            string ret = window.getMSG();
            window.log.Text += ret;

            if (ret.Contains(":)"))
            {
                MessageBox.Show("Password changed successfully");
                window.frame.NavigationService.Navigate(new SignIn(window));
            }
            else
            {
                this.Bd.Background = Brushes.Red;
                MessageBox.Show("Code validation failed !!!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Bd.Background = color;
            }

        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            window.dc();
            System.Windows.Application.Current.Shutdown();
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            window.frame.NavigationService.Navigate(new ForgotPassword(window));
        }

        private void Code_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
