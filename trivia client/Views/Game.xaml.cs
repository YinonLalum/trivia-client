﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace trivia_client.Views
{
    /// <summary>
    /// Interaction logic for Game.xaml
    /// </summary>
    public partial class Game : UserControl
    {
        public Menu menu;
        private int timePerQuestion;
        private int questionCount;
        private int currectQuestion;
        private Thread timeT;
        private int currentScore;
        private List<Button> buts;
        public Game(Menu menu,RoomData data)
        {
            this.menu = menu;
            InitializeComponent();
            menu.window.Height = this.Height;
            menu.window.Width = this.Width;

            this.Username.Content = menu.Username.Content;
            this.timePerQuestion = data.time;
            this.name.Content = data.name;
            this.questionCount = data.questions;
            this.currectQuestion = 0;
            this.currentScore = 0;
            buts = new List<Button>();
            buts.Add(ans0);
            buts.Add(ans1);
            buts.Add(ans2);
            buts.Add(ans3);
            score.Content = "Score: " + currentScore + "/" + this.questionCount;
            CycleQuestion();
        }

        private void waitForRes()
        {
            System.Threading.Thread.Sleep(2000);
            for (int i = 0; i < buts.Count; i++)
            {
                this.Dispatcher.Invoke(() =>
                {
                    buts[i].Style = this.FindResource("RoundCorner2") as Style;
                    buts[i].IsEnabled = true;
                });
            }
            this.Dispatcher.Invoke(() =>
            {
                CycleQuestion();
            });
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            timeT.Abort();
            menu.window.SendMSGByProt("{}", 17);
            string resp = menu.window.getMSG();

            if(resp.Contains(":("))
                MessageBox.Show(resp, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            menu.window.Height = 600;
            menu.window.Width = 500;
            menu.window.frame.NavigationService.Navigate(menu);
        }

        private void CycleQuestion()
        {
            QuestionResp question;
            currectQuestion++;
            if (currectQuestion > questionCount)
            {
                menu.window.SendMSGByProt("{}", 15);
                string asJson = menu.window.getMSG();
                List<PlayerResult> results = new JavaScriptSerializer().Deserialize<List<PlayerResult>>(asJson);

                menu.window.frame.NavigationService.Navigate(new Statistics(menu, results));
            }
            else
            {
                menu.window.SendMSGByProt("{}", 16);
                string asJson = menu.window.getMSG();
                question = new JavaScriptSerializer().Deserialize<QuestionResp>(asJson);

                Shuffle.ShuffleList<Button>(buts);

                buts[question.answers[0].id].Content = question.answers[0].answer;
                buts[question.answers[1].id].Content = question.answers[1].answer;
                buts[question.answers[2].id].Content = question.answers[2].answer;
                buts[question.answers[3].id].Content = question.answers[3].answer;

                buts[question.answers[0].id].Tag = question.answers[0].id;
                buts[question.answers[1].id].Tag = question.answers[1].id;
                buts[question.answers[2].id].Tag = question.answers[2].id;
                buts[question.answers[3].id].Tag = question.answers[3].id;

                this.question_count.Content = "Question: " + currectQuestion + "/" + questionCount;
                this.question.Content = question.question;
                timeT = new Thread(handleTime);
                timeT.Start();
            }
        }

        private void SubmitAns(object sender, RoutedEventArgs e)
        {
            Button but = (Button)sender;
            timeT.Abort();
            menu.window.SendMSGByProt("{\"answerid\": "+ but.Tag +"}", 14);
            string asJson = menu.window.getMSG();
            CorrectAns correct = new JavaScriptSerializer().Deserialize<CorrectAns>(asJson);
            int correctID = correct.correct;
            if(correctID == (int)but.Tag)
            {
                currentScore++;
                score.Content = "Score: " + currentScore + "/" + this.questionCount;
            }
            for(int i = 0; i<buts.Count;i++)
            {
                if((int)buts[i].Tag == correctID)
                {
                    buts[i].Style = this.FindResource("CorrectBut") as Style;
                }
                else
                {
                    buts[i].Style = this.FindResource("WrongBut") as Style;
                }
            }
            for (int i = 0; i < buts.Count; i++)
            {
                buts[i].IsEnabled = false;
            }
            Thread t = new Thread(waitForRes);
            t.Start();
        }

        private void handleTime()
        {
            int timeLeft = this.timePerQuestion;
            while(timeLeft != 0)
            {
                try
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        this.Time.Content = "Time left: " + timeLeft;
                    });
                }
                catch (Exception)
                {
                    return;
                }
                System.Threading.Thread.Sleep(1000);
                timeLeft--;
            }
            try
            {
                Application.Current.Dispatcher.Invoke((Action)delegate {
                    Button but = new Button();
                    but.Click += SubmitAns;
                    but.Tag = -1;
                    SubmitAns(but, new RoutedEventArgs());
                });
            }
            catch (Exception)
            {
                return;
            }
        }

        private class Answer
        {
            public int id { get; set; }
            public string answer { get; set; }
        }

        private class QuestionResp
        {
            public string question { get; set; }
            public List<Answer> answers { get; set; }
        }

        private class CorrectAns
        {
            public int correct { get; set; }
        }
    }
    public class PlayerResult
    {
        public string username { get; set; }
        public int correctAnswerCount { get; set; }
        public int wrongAnswerCount { get; set; }
        public float averageAnswerTime { get; set; }
    }
    public static class Shuffle
    {
        private static Random random = new Random();

        public static void ShuffleList<E>(IList<E> list)
        {
            if (list.Count > 1)
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    E tmp = list[i];
                    int randomIndex = random.Next(i + 1);

                    //Swap elements
                    list[i] = list[randomIndex];
                    list[randomIndex] = tmp;
                }
            }
        }
    }
}
