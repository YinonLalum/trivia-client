﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace trivia_client.Views
{
    /// <summary>
    /// Interaction logic for Status.xaml
    /// </summary>
    public partial class Status : UserControl
    {
        Menu menu;
        public Status(Menu menu, Stats s)
        {
            InitializeComponent();
            menu.window.Height = 600;
            menu.window.Width = 800;
            this.Username.Content = menu.Username.Content;
            this.menu = menu;
            this.average.Content = s.avg;
            this.wrong.Content = s.wrong;
            this.right.Content = s.right;
            this.games.Content = s.games;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            menu.window.Height = 600;
            menu.window.Width = 500;
            menu.window.frame.NavigationService.Navigate(menu);
        }
    }
}
